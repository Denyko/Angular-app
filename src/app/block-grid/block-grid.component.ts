import { Component, OnInit, Input } from '@angular/core';
import { articles, BlogArticle } from '../sample-data';

@Component({
  selector: 'app-block-grid',
  templateUrl: './block-grid.component.html',
  styleUrls: ['./block-grid.component.css']
})
export class BlockGridComponent implements OnInit {
  public articles: Array<BlogArticle>;
  public colsPerRow: number;

  constructor() {
  	this.articles = articles;
  	this.colsPerRow = 3;
  }

  ngOnInit() {
  }

  getArticleId(articleId: string) {
    for(let article of articles) {
      if(article.id === articleId){
        console.log(articleId, article.id);
        article.likesCount += 1;
      }
    }
  }

}
