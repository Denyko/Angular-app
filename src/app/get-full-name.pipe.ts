import { Pipe, PipeTransform } from '@angular/core';
import { BlogAuthor } from './sample-data';

@Pipe({
  name: 'getFullName'
})
export class GetFullNamePipe implements PipeTransform {

  public transform(author: BlogAuthor) {
  	return author.firstName + ' ' + author.lastName;
  }

}
