export interface BlogTag {
    id: string;
    label: string;
    category: string;
}

export interface BlogAuthor {
    id: string;
    firstName: string;
    lastName: string;
}

export interface BlogArticle {
    id: string;
    cover: string;
    title: string;
    publishedAt: Date;
    author: BlogAuthor;
    contentPreview: string;
    tags: Array<BlogTag>;
    commentsCount: number;
    likesCount: number;
}

export const articles: Array<BlogArticle> = [
    {
        id: '79b67df6ab87805fad2fd84df155b6089c6e6140',
        author: {
            firstName: 'Oralle',
            lastName: 'Brookes',
            id: 'f133968908ac5d17d79813bac62e629578111f87'
        },
        title: 'Business-focused heuristic database',
        publishedAt: new Date('2012-03-12T20:44:52Z'),
        contentPreview: 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',
        commentsCount: 45,
        likesCount: 0,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'alliance',
                id: 'cbc9ce608d03646929528257496ce2ce35ae1557',
                category: 'warning'
            },
            {
                label: 'orchestration',
                id: '9ae72f5d2e7fdd35570c6d7c3024d1981e6f4275',
                category: 'info'
            },
            {
                label: 'portal',
                id: '069072ab5a0a9cfb591440642a92d0fdb12e925a',
                category: 'info'
            },
            {
                label: 'Reduced',
                id: '0e394be72a6c64f85ce9505952ac981adc9aafc9',
                category: 'primary'
            },
            {
                label: 'intranet',
                id: '46277831e566269ef24b88bc5d7538b6f33dec5c',
                category: 'secondary'
            }
        ]
    }, {
        id: '0f2e2cf4f112569c4b1b4a52cdc9f7bdd3c38257',
        author: {
            firstName: 'Immanuel',
            lastName: 'Inge',
            id: '888861ef047c87f05d941fe677b2849d45e30717'
        },
        title: 'Managed neutral application',
        publishedAt: new Date('2001-12-18T08:37:21Z'),
        contentPreview: 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',
        commentsCount: 0,
        likesCount: 37,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'customer loyalty',
                id: '722b7ccba2f057f06adcb462df9ff51628213efb',
                category: 'dark'
            },
            {
                label: 'benchmark',
                id: 'ca639f9e16fcd549eb89f8bf06a8a63eaf93f4a9',
                category: 'light'
            },
            {
                label: 'encompassing',
                id: 'a49a1e3c5850e898cc44ddd90462beaf0e9554ec',
                category: 'light'
            }
        ]
    }, {
        id: 'aa39da8f60ce78429fd57fe6453632055347954f',
        author: {
            firstName: 'Hannis',
            lastName: 'Ferie',
            id: 'acaceb72078f3e3fa2224192be55d72a9c3fa2ff'
        },
        title: 'Seamless hybrid instruction set',
        publishedAt: new Date('2005-05-08T17:24:25Z'),
        contentPreview: 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',
        commentsCount: 14,
        likesCount: 15,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'Enhanced',
                id: 'be6e96c825e007722e7eaf06b415c11949cd9dbf',
                category: 'warning'
            }
        ]
    }, {
        id: '26bfe825f7d67934e8718802b4dd816415988261',
        author: {
            firstName: 'Jewell',
            lastName: 'Rickman',
            id: 'da2260218136ea6387951be66ce26e298628427b'
        },
        title: 'Cross-platform fresh-thinking access',
        publishedAt: new Date('2016-09-12T08:42:00Z'),
        contentPreview: 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',
        commentsCount: 100,
        likesCount: 7,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'Virtual',
                id: '38a43a5907c9b2570a8bc113607b4cb67a2230c3',
                category: 'secondary'
            },
            {
                label: 'incremental',
                id: '44158b2bb72939830194f0ba0932f975e8e8aec6',
                category: 'dark'
            },
            {
                label: 'Mandatory',
                id: '99bf63855544da6543d9e4960a1549c82215c9d2',
                category: 'dark'
            },
            {
                label: 'implementation',
                id: '7b80fd8309429d34011d67976ba9cb58aaeac970',
                category: 'warning'
            }
        ]
    }, {
        id: 'f2482304a37d5df43f630ecdec9ea0399eaa1f73',
        author: {
            firstName: 'Rance',
            lastName: 'Erbe',
            id: 'e512bc0c0341736b198c61145480ee4a34c12c5a'
        },
        title: 'Compatible high-level intranet',
        publishedAt: new Date('2001-03-31T21:31:58Z'),
        contentPreview: 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
        commentsCount: 86,
        likesCount: 41,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'hybrid',
                id: 'ade3b0da54f945e0cbf341b58590fa8ae2712d2c',
                category: 'danger'
            }
        ]
    }, {
        id: 'd185c70e78c69ddce4238544cc7c966869ea7ac1',
        author: {
            firstName: 'Neville',
            lastName: 'Woodage',
            id: '8f7d0f0e3514a59c1f2443a04853f8b4b531daf2'
        },
        title: 'Optional incremental workforce',
        publishedAt: new Date('2017-06-17T02:49:40Z'),
        contentPreview: 'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.',
        commentsCount: 44,
        likesCount: 58,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'background',
                id: '13b141630ff2a10638b2fe05886235fb9c1d10ba',
                category: 'info'
            }
        ]
    }, {
        id: 'b038e5daacbf3a6a9f4e5ad5a19ee870b36e6ba9',
        author: {
            firstName: 'Xenia',
            lastName: 'Pearde',
            id: '9fb59832a0fbff1bdfe79cd8dc6e5dd9acf77feb'
        },
        title: 'Upgradable next generation core',
        publishedAt: new Date('2000-09-24T10:44:14Z'),
        contentPreview: 'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.',
        commentsCount: 53,
        likesCount: 66,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'leading edge',
                id: '8eee576ae048e99b9efac51f184bbc3286b871cb',
                category: 'info'
            },
            {
                label: 'context-sensitive',
                id: '7704fe0412caedf9f4ea0300dd941f2ef8bd0a5f',
                category: 'danger'
            },
            {
                label: 'portal',
                id: '5c651c954e1312267e3d4a7e3250c588eed06774',
                category: 'warning'
            },
            {
                label: '24 hour',
                id: '5fa4a785e06b6ee2793dc60f5d306f3efd80c153',
                category: 'info'
            }
        ]
    }, {
        id: '8f31fe5e9de7fb22c1d20b368e5003a0c7f0e766',
        author: {
            firstName: 'Romain',
            lastName: 'Nyland',
            id: 'a0a273956a5d8a52bb7d66eac852f8b181827e84'
        },
        title: 'Customizable empowering neural-net',
        publishedAt: new Date('2011-05-21T02:31:11Z'),
        contentPreview: 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',
        commentsCount: 19,
        likesCount: 41,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'Exclusive',
                id: '25c277bcf93af11c0628bddc958764888485b243',
                category: 'secondary'
            },
            {
                label: 'pricing structure',
                id: '510b7bf978804356dde773e4cb143aaaed99d2ea',
                category: 'danger'
            },
            {
                label: 'even-keeled',
                id: '58d2cd38cb233265d3a8d1fa517205a69def6e67',
                category: 'dark'
            },
            {
                label: 'extranet',
                id: '74a916a780db8cfc1e55aabec5c4235fa141b66f',
                category: 'info'
            },
            {
                label: 'collaboration',
                id: 'd33acfbf4da5e1799768350283a8f93e2fefca0e',
                category: 'primary'
            }
        ]
    }, {
        id: '389a80689db763d310a17c7b7843cde9dc562bfd',
        author: {
            firstName: 'Jo',
            lastName: 'Vannoort',
            id: '2dc3e382ebd68ec40703e860972a181e1c71a4fd'
        },
        title: 'Ergonomic holistic strategy',
        publishedAt: new Date('2009-02-09T11:01:00Z'),
        contentPreview: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.',
        commentsCount: 60,
        likesCount: 100,
        cover: 'https://source.unsplash.com/random',
        tags: [
            {
                label: 'Reduced',
                id: '1a7cddee0c9b2b40c1e02bf9298d6f6086ff6689',
                category: 'warning'
            }
        ]
    }
]