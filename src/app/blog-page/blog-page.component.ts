import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.css']
})
export class BlogPageComponent implements OnInit {
  @Input() articles;
  @Input() page;

  constructor() { }

  ngOnInit() {
  }

}
