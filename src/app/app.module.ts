import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { GroupElementsByPipe } from './group-elements-by.pipe';
import { GetFullNamePipe } from './get-full-name.pipe';
import { BlogCardComponent } from './blog-card/blog-card.component';
import { BlogPageComponent } from './blog-page/blog-page.component';
import { BlockGridComponent } from './block-grid/block-grid.component';



@NgModule({
  declarations: [
    AppComponent,
    GroupElementsByPipe,
    GetFullNamePipe,
    BlogCardComponent,
    BlogPageComponent,
    BlockGridComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
