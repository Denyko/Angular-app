import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';

@Component({
  selector: 'app-blog-card',
  templateUrl: './blog-card.component.html',
  styleUrls: ['./blog-card.component.css']
})
export class BlogCardComponent implements OnInit { 

  @Input() article;
  @Output() public liked: EventEmitter<string>;

  constructor() {
  	this.liked = new EventEmitter();
  }

  ngOnInit() {

  }

  public handleClick() {
    this.liked.emit(this.article.id);
  }

}
