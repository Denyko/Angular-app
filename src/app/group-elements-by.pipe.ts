import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'groupElementsBy'
})

export class GroupElementsByPipe implements PipeTransform {
  
  public transform(arr: Array<any>, size: number): any {
  	if (arr == null || !Array.isArray(arr)) {
  		throw new Error('Missing Array');
  	}
  	if (size == null || Number.isNaN(Number(size)) || size < 1) {
  		throw new Error(`Invalid size '${size}'`);
  		
  	}
    const result = [];

  	for (let index = 0; index < arr.length; index += size) {
        let item = arr.slice(index, index + size);
        result.push(item);
    }
    return result;
  }
}

